# [Introduction to NodeJS](https://www.edx.org/course/introduction-to-nodejs-0)

About this course Skip Course Description
Have you ever wanted to create a full-fledged web application, beyond just a simple HTML page? In this course, you will learn how to set up a web server, interact with a database and much more!

This course will start off by teaching you the basics of Node.js and its core modules. You will then learn how to import additional modules and configure your project using npm. From there, you will learn how to use Express to set up a web server and how to interact with a MongoDB database using Mongoose. By the end of the course you will have created several real-world projects such as a web scraper, a blogging API, and a database migration script.

## What you'll learn

* How to set up a Node.js project using npm
* How to use the Node.js core modules
* How to use Express to set up a web server
* How to use MongoDB to store data in a database
* How to use Mongoose to model database schemas

## Course Syllabus

Skip Syllabus DescriptionModule 0:
Brief overview on the benefits of using Node.js and how Node.js is used in modern web development.

### Module 1:

Introduction to setting up a Node.js project and importing modules using npm, as well as using core modules to make HTTP requests and manipulate the file system. The module labs will have you build a web crawler and a CSV file to JSON file converter.

### Module 2:

Introduction to using the Express framework to set up a web server, as well as implementing API routing, middleware, and URL parameters.
The module labs will have you build a REST API for a blog using Express.

### Module 3:

Introduction to setting up a MongoDB database and connecting it to a Node.js server.
The module labs will have you build a REST API that stores data in a MongoDB database. You will also build a node script to migrate data from JSON files to a MongoDB database.

### Module 4:

Introduction to using Mongoose to model database schemas and interact with MongoDB databases easier.
The module labs will have you build relational queries using Mongoose. You will also reimplement your REST API from module 2 using Mongoose as a database library.