const http = require('http')
const url = 'http://nodeprogram.com'
//
http.get(url, (response) => {
    let c = 0
  response.on('data', (chunk) => {
    c++
    console.log(chunk.toString('utf8'))
  })
  response.on('end', () => {
    console.log(`response has ended with ${c} chunk(s)`)
  })
}).on('error', (error) => {
  console.error(`Got error: ${error.message}`)
})
// const http = require('http')
// const url = 'http://nodeprogram.com'

// http.get(url, (response) => {
//   let rawData = ''
//   response.on('data', (chunk) => {
//     rawData += chunk
//   })
//   response.on('end', () => {
//     console.log(rawData)
//   })
// }).on('error', (error) => {
//   console.error(`Got error: ${error.message}`)
// })