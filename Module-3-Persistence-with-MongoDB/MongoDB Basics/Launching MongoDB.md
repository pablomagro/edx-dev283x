# Launching MongoDB

Before you can launch the MongoDB instance, please make sure you have installed it. The detailed instructions are in the beginning of this course.

Launch the mongod service with:

```bash
mongod
```

You should be able to see information in your terminal. The default port is 27017. If you see a data folder error, then you might need to create and configure your /data/db folder so it can be accessed by the MongoDB process. To do so, run this command to create a folder:

```bash
sudo mkdir -p /data/db
```

And then change owner permissions to mongodb:

```bash
sudo chown -R mongodb /data/db
```

or you can pull your current username with the whoami command and assign permissions to that user:

```bash
sudo chown -R $(whoami) /data/db
```