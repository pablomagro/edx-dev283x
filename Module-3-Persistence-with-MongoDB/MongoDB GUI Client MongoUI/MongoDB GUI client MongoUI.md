# MongoDB GUI client MongoUI

MongoUI is an open-source web and desktop app which allows you to administer local and remote MongoDB instances via GUI. No need to type commands in a terminal anymore. The convenient interface will allow to create, update, remove and filter/search documents. You can switch between collections or even databases with just a single click.

It is currently a standalone tool (run as an app locally or on your server). This is a brand new v2 of MongoUI. The old version uses DerbyJS, but the new version uses Webpack, React, React Router, React Bootstrap and of course Express and Node.

## Installation

Execute the npm install -g mongoui command in any folder in the Terminal/Command Prompt to install MongoUI globally:

```bash
npm i -g mongoui
```

Run the following command to launch MongoUI:

```bash
mongoui
```

Open the browser at localhost:3001 if it hasn't been opened automatically. You need to have a database instance running (mongod) to access the data. Once you have a database instance running, click on the database you want to use and then on the collection you want to work with.

