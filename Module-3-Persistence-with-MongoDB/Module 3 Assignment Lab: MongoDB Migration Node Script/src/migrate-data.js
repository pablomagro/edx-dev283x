// PERSIST

const mongodb= require('mongodb')
const client = mongodb.MongoClient
// URI connection
const url = 'mongodb://localhost:27017/edx-course-db'

const insertMigratedCustomer = (documents) => {
  mongodb.MongoClient.connect(url, (error, db) => {
    return new Promise((resolve, reject) => {
      if (error) return reject(error)

      insertDocuments(db, documents)
        .then(result => resolve(result))
        .catch(error => reject(error))
    })
  })

  const insertDocuments = (db, documents) => {
    // Get reference to edx-course-customers collection
    const collection = db.collection('edx-course-customers')

    return new Promise((resolve, reject) => {
      collection.insert([
        documents
      ], (error, result) => {
        if (error) return reject(error)
        // console.log(result);
        resolve(result)
      })
    })
  }
}

// READ

const incompleteCustomers = require('./m3-customer-data.json')
const customerAddresses = require('./m3-customer-address-data.json')

// READ and validate in parameter.

const objectsNumber =  process.argv[2]

if (!objectsNumber || objectsNumber < 1
    || objectsNumber > incompleteCustomers.length
      || incompleteCustomers.length !== customerAddresses.length) {
  console.info('Provide a valid number of object to process.')
  process.exit(2) // Invalid in parameter.
}
// console.log(objectsNumber);

// Build queries per objects

const queries = [], customersLength = incompleteCustomers.length || 0
let start = 0, end = objectsNumber
let documents, count = 0
let remainingCustomers = incompleteCustomers.length;

// while (remainingCustomers > 0) {
//   documents = incompleteCustomers.splice(start, end).map((c, idx) => Object.assign(c, customerAddresses[idx]))
//   console.log('documents :', documents);
//   start += objectsNumber
//   end += objectsNumber
// //   queries.push(getCustomerChunk(idx++, chunkSize));
//   remainingCustomers -= objectsNumber;
//   // console.log('count :', ++count);
//   queries.push(insertMigratedCustomer(documents))
// }

const parallel = (db, start, end) => {
  return new Promise((resolve, reject) => {
    console.log(`Processing ${start}-${end} out of ${incompleteCustomers.length}`)

    return db.collection('customers').insert(incompleteCustomers.slice(start, end), (error, result) => {
      if (error) reject(error)
      resolve(result)
    })
  })
}

const handleError = (error, exit = 1) => {
  console.error(error)
  return process.exit(exit)
}

mongodb.MongoClient.connect(url, (error, db) => {
  if (error) handleError(error)

  console.log('customersLength :', customersLength);

  for (let index = 0; index < customersLength; index++) {
    // console.log('index :', index);
    incompleteCustomers[index] = Object.assign(incompleteCustomers[index], customerAddresses[index])
    // console.log('customers[index] :', incompleteCustomers[index]);

    if (index % objectsNumber == 0) {
      const start = index
      const end = (start+objectsNumber > customersLength) ? customersLength-1 : start+objectsNumber

      queries.push(parallel(db, start, end))
    }
  }

  console.log('queries :', queries);

  // customers.forEach((customer, index, list) => {
  //   customers[index] = Object.assign(customer, customerAddresses[index])

  //   if (index % objectsNumber == 0) {
  //     const start = index
  //     const end = (start+objectsNumber > customers.length) ? customers.length-1 : start+objectsNumber

  //     tasks.push((done) => {
  //       console.log(`Processing ${start}-${end} out of ${customers.length}`)
  //       db.collection('customers').insert(customers.slice(start, end), (error, results) => {
  //         done(error, results)
  //       })
  //     })
  //     queries.push(parallel(start, end))
  //   }
  // })

  console.log(`Launching ${queries.length} parallel task(s)`)
  const startTime = Date.now()

  // async.parallel(tasks, (error, results) => {
  //   if (error) console.error(error)
  //   const endTime = Date.now()
  //   console.log(`Execution time: ${endTime-startTime}`)
  //   db.close()
  // })

  Promise.all(queries)
    .then(result => {
      const endTime = Date.now()
      console.log(`Execution time: ${endTime-startTime}`)
      db.close()
      process.exit(0)
    })
    .catch(error => {
      console.error(error)
      process.exit(1)
    })
})
