# MongoDB Overview

This module will teach development with MongoDB. Here's the overview of the module.

## MongoDB Basics

1. Launching MongoDB
1. MongoDB REPL

## MongoDB Native Driver

1. Connecting to MongoDB from Node using MongoDB Native Driver
1. Working with MongoDB Native Driver

## MongoDB GUI Client MongoUI

## Summary of Module 3: Persistence with MongoDB

## Module 3 Tutorial Lab: CRUD REST API with Node, Express and MongoDB

## Module 3 Assignment Lab: MongoDB Migration Node script

## Module 3 Assessment