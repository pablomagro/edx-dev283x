# C.A.P. theorem

CAP stands for:

* **C**onsistency (strong vs. eventual-delay)
* **A**vailability
* **P**artition tolerance (on cluster)

The theorem says you cannot have all three but you can have any two of them. In SQL databases you get C+A but in NoSQL databases you get A+P. AP allows for better scaling across multiple partitions but not all data will be consistent at all the times.

In some cases, consistency is not entirely crucial. Think about Facebook status updates for a moment. Does it really matter that some friends see a status update a few seconds or minutes later? Unless it's a breaking news, no. It doesn't matter. Facebook friends in Japan will see updates from their American friends a little bit slower than American friends but they'll see it eventually.

On the contrary, you want to have your banking balance to be consistent across all instances no matter what, regardless of whether you log in from a smartphone or desktop.

## No SQL!

Key characteristics of NoSQL (such as MongoDB):

* A+P from C.A.P.
* No relationships in the database.
* Redundancy is okay and sometime even good.

## NoSQL Databases

There are many types of NoSQL databases:

* Key-value stores (Redis, think hash tables)
* Document stores (mongoDB, think JSON)
* Columnar stores (hbase, average age)
* Graphs stores (neo4j)

## SQL vs. NoSQL

Relation DB->normilized for any query, no biases

NoSQL->biases to specific query patterns that we have

## MongoDB

MongoDB is a document store NoSQL database. It's great at being distributed and scaling.