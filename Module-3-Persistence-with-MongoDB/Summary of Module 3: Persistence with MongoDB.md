# Summary of Module 3: Persistence with MongoDB

In this module you've learned how to launch MongoDB, use REPL, and work with it from Node. Key things to remember:

* REPL is launched with mongo while a database instance with mongod
* The MongoDB native driver has methods which support error-first callbacks: find(), update(), remove(), and insert()
* MongoUI can be installed with npm i -g mongoui and launched with mongoui