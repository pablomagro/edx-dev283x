# Input Validation

It is very important to validate the incoming data. Never trust the client. The data can be malformed causing your app to crash or just malicious on purpose if a client is an attacker.

A manual validation can be done in each route which accepts data. If it's in the request body, you can use an if/else statement:

```js
app.post('/login', (req, res) => {
  if (!req.body.email || !req.body.password)
    return res.send({
      error: 'Please enter your email and password.'
    })
  if (!validateEmail(req.body.email) || ! validatePassword(req.body.password))
    return res.send({
      error: 'Invalid format for email and/or password.'
    })
  login(req.body.email, req.body.password)
})
```

A better way is to use express-validator because it allows you to use a schema.

* **White listing**
let obj = {
    a = req.body.a,
    b = req.body.b
}
* **trim()**
* **express-validator**