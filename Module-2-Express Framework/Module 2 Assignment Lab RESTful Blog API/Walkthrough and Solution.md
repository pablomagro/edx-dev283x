#  Walkthrough and Solution

You can download the solution here.

This is the main server file that should be in your project root:

[m2-assignment-server.js](./m2-assignment-server.js)

These files should be in the 'routes' folder of your project:

[posts.js](./posts.js)

[comments.js](comments.js)

[index.js](index.js)

To run the server, make a new project using npm init. Next, download the server and move it to the root of your project. Next, download posts.js, comments.js, and index.js and put them in the routes folder of your project. Make sure to download all dependencies with npm install. You can run the server with 'node m2-assignment-server.js'.

## My solution

https://github.com/pablomagro/edx-introduction-to-nodejs-module-2-restful-blog-api