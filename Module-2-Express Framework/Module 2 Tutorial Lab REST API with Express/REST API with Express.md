# REST API with Express

## Module 2 Tutorial: REST API with Express

In this tutorial lab, you will build a REST API with Express. For the purposes of this tutorial, the data will be stored in the memory of the server instead of a database. This lab will use concepts and skills learned in the module 2 such as express routing, middleware, and query parameters.

## REST API with Express

In modern web development APIs play a huge role. It's very important to know how to build them. In this lab, you'll build a RESTful API using the Express framework.

The REST API server will have the following four endpoints:

1. GET /accounts to retrieve a list of accounts
1. POST /accounts to create a new account
1. PUT /accounts to update an account
1. DELETE /accounts to remove an account

Each endpoint is a server route, i.e., clients can make HTTP requests and the server will respond.

The server implementation consists of the following steps:

1. Create a new folder, package.json and install packages
1. Create server.js with an Express app
1. Implement four endpoints in the Express app
1. Test the server

Let's get started with a fresh folder in which you will implement the server. Open your terminal application or command prompt, and execute the following commands:

```bash
mkdir rest-api
cd rest-api
```

Then, create package.json either manually with your favorite code editor or with the npm init command:

```bash
npm init -y
```

Our project will leverage a few npm modules which are Express middleware (plugins):

* **morgan**: logging of the server requests in various formats for debugging, auditing and other purposes
* **body**-parser: parsing of the incoming request body/payload into a Node object from a string or other formats
* **errorhandler**: basic user friendly error messaging

Install the three packages which will be your project dependencies:

```bash
npm i express morgan errorhandler -E
```

Now everything is ready for the implementation of the server. Create a new file server.js in the newly created folder (project root). The server.js file must be on the same folder level as your package.json and node_modules.

In the server.js file, start importing the dependencies using const and require():

```js
const express = require('express')
const logger = require('morgan')
const errorhandler = require('errorhandler')
const bodyParser = require('body-parser')
```