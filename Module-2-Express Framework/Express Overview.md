# Express Overview

Express.js is a web framework based on the core Node.js http module and Connect components. The components are called middleware and they are the cornerstones of the framework philosophy configuration over convention. In other words, Express.js systems are highly configurable, which allows developers to freely pick whatever libraries they need for a particular project. For these reasons, the Express.js framework leads to flexibility and high customization in the development of web applications.

This module will build a solid foundation of Express development by teaching the following topics:

## Why Express and Express Installations

1. Why Express?
1. Express and Express Generator Installation

## Hello World with Express

1. Hello World with Express
1. Typical Express Project Structure
1. Typical Express app Structure

## Creating and Using Middleware

1. Middleware
1. Creating Middleware
1. Parsing body
1. Useful middleware from npm

## Implementing REST API Routing

1. Implement REST API routing
1. Handling various HTTP requests
1. Request object
1. Response object

## URL Parameters, Query Strings and Input Validation

1. Defining and accessing URL parameters
1. Accessing query string data
1. Input validation