# Hello World with Express

Create a new folder and create a package.json in it:

```
mkdir express-hello-world
cd express-hello-world
npm init -y
```
Then create a new file named app.js using your favorite editor (mine is VS Code!). Next, type the following code to create an Express app and define a single route (endpoint):

```js
const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.send('hello world')
})

app.listen(3000)
```

Save the file and navigate to your newly created folder using the terminal. Launch the node script with:

```
node app.js
```

Once the application is launched, leave the terminal window open and open your favorite browser at localhost:3000. You should see hello world.

Congratulations. Your first Express server is working.