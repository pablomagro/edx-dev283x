# Implementing REST API Routing

Servers must have routes, otherwise they are not useful at all. As a very basic example, consider this Express route which serves Hello World string to requests made to / (root address):

```js
app.get('/', (req, res) => {
  res.send('Hello World')
})
```

You can notice that app.get() is referring to the GET HTTP method. That's what browsers will use navigate to a URL in a browser.

The first argument is a URL string. It could be a regular expression as well. The second argument is a request handler with request and response objects.

If you have two routes in app.js:

```js
const {homePage, getUsers} = require('./routes')

app.get('/', homePage)
app.get('/users', getUsers)
```

The first one basically takes care of all the GET requests to the home page (/), such as http://localhost:3000/ and triggers the homPage method. The second takes care of requests to /users, such as http://localhost:3000/users and triggers the getUsers method.

Both of the routes process URLs in a case-insensitive manner and in the same way with trailing slashes.

204: when updated and delete